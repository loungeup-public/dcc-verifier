const path = require('path');
const BundleAnalyzerPlugin =
  require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  mode: 'production',
  watch: false,
  entry: {
    'verifier.min': './src/verifier.ts',
    'test': './src/test.ts',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: {
      name: 'dccVerifier',
      type: 'umd',
    },
    clean: true,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '*.json'],
    fallback: {},
  },
  plugins: [
    /* new BundleAnalyzerPlugin({
      defaultSizes: 'stat',
    }), */
  ],
  devtool: 'source-map',
  optimization: {
    usedExports: true,
    mangleWasmImports: true,
    removeAvailableModules: true,
    mangleExports: true,
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
};
