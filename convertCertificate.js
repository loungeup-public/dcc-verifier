import { X509Certificate } from '@peculiar/x509';
import { writeFile } from 'fs/promises';
import { join } from 'path';
import { webcrypto } from 'cosette/build/sign.js';

const cert =
  'MIICljCCAfegAwIBAgIUSYY9bnQzVLAryJ1ROotKecLFIZUwCgYIKoZIzj0EAwIwOTEbMBkGA1UEAwwSVGVzdCBHRU8gREdDRyBDU0NBMQ0wCwYDVQQKDARQU0RBMQswCQYDVQQGEwJHRTAeFw0yMTA5MjgxNDMyMzVaFw0yMzA5MjgxNDMyMzVaMFIxGzAZBgNVBAMMEmVIZWFsdGggRG9jIFNpZ25lcjEmMCQGA1UECgwdTWluaXN0cnkgb2YgSGVhbHRoIG9mIEdlb3JnaWExCzAJBgNVBAYTAkdFMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEdbrCbLgPZnv3JWkoA7m4Qpc+kG9mhbl98w/DZs/e2BGH2auRZXP2U3ajzqxZhSsi6YO5Pi4nzab6nRQrUWiqeKOBwzCBwDAfBgNVHSMEGDAWgBS6FY6INXaxalc0y7LDhhE8dPXJpTAwBgNVHSUEKTAnBgsrBgEEAY43j2UBAwYLKwYBBAGON49lAQEGCysGAQQBjjePZQECMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly90ZXN0LW9jc3AuY3JhLmdlL3Rlc3RnZW9kZ2NnY3NjYS5jcmwwHQYDVR0OBBYEFGixycqiTeWapfAVq63ZtUrE1FojMA4GA1UdDwEB/wQEAwIHgDAKBggqhkjOPQQDAgOBjAAwgYgCQgGzGl7p/MlzPS9+2K0m6WAogQZr8p4mO5gUFABrw9N7CQ5p53MKq/YZEasEE72seE1vY32O5vjH+t6ueLlBlzKmzQJCAYYdnV+qI5ukNjgag4UmZQqof8LlXwywPonj/Hqsz/kIcniVeASzMFBNiGl6U03wgdDaXfY/rbb+jQnT5VVL/a91';

async function convertCert(data) {
  //const pem = new TextDecoder().decode(data);

  const x509cert = new X509Certificate(data);

  const kid = Buffer.from(await x509cert.getThumbprint('SHA-256', webcrypto))
    .slice(0, 8)
    .toString('base64');

  const result = {
    [kid]: {
      serialNumber: x509cert.serialNumber,
      subject: x509cert.subject,
      issuer: x509cert.issuer,
      notBefore: x509cert.notBefore.toISOString(),
      notAfter: x509cert.notAfter.toISOString(),
      signatureAlgorithm: x509cert.signatureAlgorithm.name,
      fingerprint: Buffer.from(await x509cert.getThumbprint(webcrypto)).toString(
        'hex'
      ),
      ...(await exportPublicKeyInfo(x509cert.publicKey)),
    },
  };
  console.log(result);

  await writeFile(join('src', 'certs.json'), JSON.stringify(result, null, 4));
}

async function exportPublicKeyInfo(publicKey) {
  const public_key = await publicKey.export(webcrypto);
  const spki = await webcrypto.subtle.exportKey('spki', public_key);

  // Export the certificate data.
  return {
    publicKeyAlgorithm: public_key.algorithm,
    publicKeyPem: Buffer.from(spki).toString('base64'),
  };
}

convertCert(cert);
