
declare module 'cose-js' {
    export var common: typeof import("cose-js/lib/common");
    export var mac: typeof import("cose-js/lib/mac");
    export var sign: typeof import("types/cose-js/lib/sign");
    export var encrypt: typeof import("cose-js/lib/encrypt");
}
