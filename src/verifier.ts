import { parse as parse2ddoc } from './2ddocVerifier';
import {testDCC, verify as verifyDcc } from './dccVerifier';

interface ValidatorOutput {
	valid: boolean;
	success: boolean;
}

interface ValidatorArg {
	targetDate: Date
}

export async function validate(data: string, args: ValidatorArg, fn: (data: any, args: ValidatorArg) => ValidatorOutput) {
	let certificateInfos = undefined;
	if (testDCC(data)) {
		certificateInfos = await verifyDcc(data);
	} else {
		certificateInfos = await parse2ddoc(data);
	}
	return fn(certificateInfos, args);
}

export default validate;
