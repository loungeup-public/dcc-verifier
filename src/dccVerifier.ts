import Ajv from 'ajv/dist/2020';
import { decode as b45decode } from 'base45-ts';
import {
  fromByteArray as b64encode,
  toByteArray as b64decode,
} from 'base64-js';
import type { Verifier } from 'cosette/build/sign';
import { cbor, verify as coseVerifiy, webcrypto,  } from 'cosette/build/sign';
// @ts-ignore
import { inflate } from 'pako/lib/inflate.js';

// TODO import certs from INGroup api
import certs from './certs.json';

import dccFrChema from './dcc-fr-schema.json';
import dccSchema from './dcc-schema.json';
import { HCert } from './dcc_types';
export const DCC_PREFIX = 'HC1:';
export const DCC_FR_PREFIX = 'HCFR1:';
export const EX_PREFIX = 'EX1:';

const schema = {
  en: dccSchema,
  fr: dccFrChema,
};

const CWT_CLAIMS = Object.freeze({
  ISSUER: 1,
  EXPIRATION: 4,
  ISSUED_AT: 6,
  HCERT: -260,
});

interface DSC {
  serialNumber: string;
  subject: string;
  issuer: string;
  notBefore: string;
  notAfter: string;
  signatureAlgorithm: string;
  fingerprint: string;
  publicKeyAlgorithm:
    | AlgorithmIdentifier
    | RsaHashedImportParams
    | EcKeyImportParams;
  publicKeyPem: string;
}

export interface DCC {
  hcert: HCert;
  kid: string;
  issuer: string | null;
  issuedAt: number | null;
  expiresAt: number | null;
  certificate: DSC;
}

function removePrefix(data: string): { data: string; schema: string } {
  if (data.startsWith(DCC_PREFIX)) {
    return { data: data.substring(DCC_PREFIX.length), schema: 'en' };
  } else if (data.startsWith(DCC_FR_PREFIX)) {
    return { data: data.substring(DCC_PREFIX.length), schema: 'fr' };
  } else if (data.startsWith(EX_PREFIX)) {
    return { data: data.substring(DCC_PREFIX.length), schema: 'fr' };
  } else {
    return { data, schema: 'en' };
  }
}

export function testDCC(data: string) {
  return (
    data.startsWith(DCC_PREFIX) ||
    data.startsWith(EX_PREFIX) ||
    data.startsWith(DCC_PREFIX)
  );
}

export async function verify(data: string) {
  let schemaType = '';
  ({ data, schema: schemaType } = removePrefix(data));
  const buff = b45decode(data);

  let deflateBuffer;

  if (buff[0] == 0x78) {
    deflateBuffer = inflate(buff);
  }

  if (deflateBuffer == null) {
    return 'false';
  }

  let kid = '';
  let certificate = undefined as unknown as DSC;

  async function verifier(
    kid_bytes: Uint8Array,
    algorithm: Algorithm
  ): Promise<Verifier> {
    kid = b64encode(kid_bytes);

    certificate = findDCCPublicKey(kid);

    if (
      typeof certificate.publicKeyAlgorithm === 'object' &&
      certificate.publicKeyAlgorithm.name.startsWith('RSA') &&
      algorithm.name.startsWith('RSA')
    ) {
      certificate.publicKeyAlgorithm.name = algorithm.name;
    }
    const key = await getCertificatePublicKey(certificate);

    return { key };
  }

  const raw_data: Uint8Array = await coseVerifiy(deflateBuffer, verifier);


  const cborData: Map<number, any> = await cbor.decodeFirst(raw_data);
  const hcert = cborData.get(CWT_CLAIMS.HCERT)?.get(1) || {};
  const ajv = new Ajv();
  const dateValidator = (s: string) => !isNaN(Date.parse(s));

  ajv.addFormat('date', dateValidator);
  ajv.addFormat('date-time', dateValidator);
  ajv.addKeyword('valueset-uri');

  const hcertValid = ajv.validate(
    schema[schemaType as keyof typeof schema],
    hcert
  );
  if (!hcertValid) {
    const validationErrors = ajv.errors?.map((err) => err.message).join('\n');
    throw new Error(`DCC validation failed : ${validationErrors}`);
  }

  const issuer = cborData.get(CWT_CLAIMS.ISSUER);
  const issuedAt = cborData.get(CWT_CLAIMS.ISSUED_AT);
  const expiresAt = cborData.get(CWT_CLAIMS.EXPIRATION);

  const now = Math.floor(Date.now() / 1000);
  if (issuedAt && now < issuedAt) throw new Error('DCC issued in future');
  if (expiresAt && expiresAt < now) throw new Error('DCC is expired');

  return getCertificateInfo({
    hcert,
    kid,
    issuer,
    issuedAt,
    expiresAt,
    certificate,
  });
}

function findDCCPublicKey(kid: string) {
  if (!(kid in certs)) throw new Error(`Unknow certificate entity : ${kid}`);
  const certificate: DSC = certs[kid as keyof typeof certs] as DSC;
  const notAfter = new Date(certificate.notAfter);
  const notBefore = new Date(certificate.notBefore);
  // Verify that the certificate is still valid.
  const now = new Date();
  if (now > notAfter || now < notBefore)
    throw new Error(`Invalid certificate : ${certificate}`);
  return certificate;
}

async function getCertificatePublicKey({
  publicKeyAlgorithm,
  publicKeyPem,
}: DSC): Promise<CryptoKey> {
  const der = b64decode(publicKeyPem);
  const public_key = await webcrypto.subtle.importKey(
    'spki',
    der,
    publicKeyAlgorithm,
    true,
    ['verify']
  );
  return public_key;
}

function getCertificateInfo(cert: DCC) {
  const hcert = cert.hcert;
  let dob = hcert.dob;
  if (dob.search(/^(\d\d\.){2}(19|20)\d\d$/) != -1) {
    dob = dob.split('.').reverse().join('-');
  }
  const common = {
    first_name: hcert.nam.gn || (hcert.nam.gnt || '-').replace(/</g, ' '),
    last_name: hcert.nam.fn || hcert.nam.fnt.replace(/</g, ' '),
    date_of_birth: new Date(dob),
    source: { format: 'dcc' },
  } as const;
  if (hcert.v && hcert.v.length) {
    return {
      type: 'vaccination',
      uid: hcert.v[0].ci,
      vaccination_date: new Date(hcert.v[0].dt),
      vaccine: hcert.v[0].mp,
      doses_received: hcert.v[0].dn,
      doses_expected: hcert.v[0].sd,
      ...common,
    };
  }
  if (hcert.t && hcert.t.length) {
    return {
      type: 'test',
      uid: hcert.t[0].ci,
      test_date: new Date(hcert.t[0].sc),
      // 260415000=not detected: http://purl.bioontology.org/ontology/SNOMEDCT/260415000
      is_negative: hcert.t[0].tr === '260415000',
      is_inconclusive: !['260415000', '260373001'].includes(hcert.t[0].tr),
      ...common,
    };
  }
  if (hcert.r && hcert.r.length) {
    return {
      type: 'recovery',
      uid: hcert.r[0].ci,
      test_date: new Date(hcert.r[0].fr), // date of positive test
      ...common,
    };
  }
  throw new Error('Unsupported or empty certificate: ' + JSON.stringify(cert));
}
