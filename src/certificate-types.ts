import { DCC } from './dccVerifier';

export interface CommonVaccineInfo {
	type: 'vaccination';
	vaccination_date: Date;
	doses_received: number;
	doses_expected: number;
}

export interface CommonTestInfo {
	type: 'test';
	test_date: Date;
	is_negative: boolean;
	/// Set to true if the test did not give a conclusive result.
	is_inconclusive: boolean;
}

export interface CommonRecoveryInfo {
    type: 'recovery';
    test_date: Date;
}

export interface AllCommonInfo {
	uid: string;
	first_name: string;
	last_name: string;
	date_of_birth: Date;
	source: { format: 'dcc';} | { format: '2ddoc';};
}

export type CommonCertificateInfo = AllCommonInfo & (CommonVaccineInfo | CommonTestInfo | CommonRecoveryInfo);
