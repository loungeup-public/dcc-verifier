import validate from './verifier';

async function test() {
  const data =
    'HC1:NCFG.LJZ9OJ2:20ECB$7WBKG5O2/J0K-03SQ//RTPJQKL-X68H4GTAYF8%:UKL68LR%2UYZG*IP:T0JBB272MF5.Q859OEADQI4O*KXUHSJO40656UAAMBOFFB92-8RSGNETTJU9ZU1XNVKV.470$AV3OV+R$6G%RH*+B%6R+3PPFDBI1EQD-X2J+5R6KNLHI4BU51XZ3BPIED8ESMFADPS20JAD+2748GJ2H6AE7AH9F7AV8Z2ZAD-6G:+70J51AF$2H.VKKFI0*RC%3ZZHU8MMA6::DZX4S.19NBO9QCMBF*G2W08MHL.LVZ5-YM6WM+D2%-E9/0-HSMDBD3N7CTYOA5534QI/XAJ31LP0U:QU+4NVO-3SH72FGW TDLGCO%SP*44LFQJDOPFRVT9W0B$KZ D-A92P31K78ZC7 26:01E2AWVNA76ATBO8H$0BYLK7LN9T-PP*Z3JR3AC9XZLILTGM93YC/PH8B98/G2SM8GE8.7+PQ*GBA450241SIN05T$B8GB:2U/SB G1JULALB+DTM$BK76I9A9I5D3TK:L*AMTFV03OMEN93DETE+TUBRD1/EE2G3NNXZR%HDXL3*N5S3NV0SP7R2HDBLP B5ORGMM82L392OD1VP9EVFVF-N*J316N201G$9O5';
  const result = await validate(
    data,
    { targetDate: new Date() },
    (data, args) => {
      if (data.type == 'vaccination') {
        if (data.doses_received === data.doses_expected) {
          return { valid: true, success: true };
        } else {
          return { valid: true, success: false };
        }
      }
      return { valid: false, success: false };
    }
  );
  console.log(result);
}

test();
