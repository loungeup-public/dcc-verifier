# DCC Verifier

## Test
To test this lib you need to :
- go to https://github.com/eu-digital-green-certificates/dgc-testdata
- select a country
- go to the 2DCode/raw folder
- open a raw file
- from this file copy the TESTCTX->CERTIFICATE value into convertCertificate.js file
- from this file copy the PREFIX value into to src/test.ts file
- optionnal : edit the test.ts file to customize the validation function
- run `yarn run build:test` or `npm run build:test`
- open test.html in your browser
- open the console : you should an object with 2 properties, valid and succes
